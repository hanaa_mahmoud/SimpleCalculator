package com.hanaa.simplecalculator.domain.interactor

import com.hanaa.simplecalculator.data.Operation
import com.hanaa.simplecalculator.data.OperationItem
import com.hanaa.simplecalculator.domain.exception.DivideByZeroException
import com.hanaa.simplecalculator.domain.exception.InfiniteNumberException
import com.hanaa.simplecalculator.domain.exception.NotNumberException
import com.hanaa.simplecalculator.presentation.util.roundNumber

class PerformOperationUseCase{
    fun getResult(operationItem: OperationItem): Double? {
        if (operationItem.firstOperand.isNaN() || operationItem.secondOperand.isNaN()) throw NotNumberException()
        else if (operationItem.firstOperand.isInfinite() || operationItem.secondOperand.isInfinite())
            throw InfiniteNumberException()
        val result = when (operationItem.operation) {
            Operation.Plus -> operationItem.firstOperand + operationItem.secondOperand
            Operation.Minus -> operationItem.firstOperand - operationItem.secondOperand
            Operation.Multiply -> operationItem.firstOperand * operationItem.secondOperand
            Operation.Division -> {
                if (operationItem.secondOperand == 0.0) throw DivideByZeroException()
                else operationItem.firstOperand / operationItem.secondOperand
            }

        }.run {
            if (isInfinite()) throw InfiniteNumberException(true)
            roundNumber()
        }

        return result
    }
}