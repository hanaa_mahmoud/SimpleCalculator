package com.hanaa.simplecalculator.domain.exception

class NotNumberException : Exception()