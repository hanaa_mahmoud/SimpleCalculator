package com.hanaa.simplecalculator.domain.exception

class InfiniteNumberException(val isResult:Boolean=false) : Exception()