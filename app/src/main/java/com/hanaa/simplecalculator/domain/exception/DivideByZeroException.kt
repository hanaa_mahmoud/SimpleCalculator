package com.hanaa.simplecalculator.domain.exception

class DivideByZeroException : Exception()