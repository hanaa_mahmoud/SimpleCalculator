package com.hanaa.simplecalculator.data

data class OperationItem(
    val firstOperand: Double,
    val secondOperand: Double,
    val operation: Operation
) {
    val undoOperation: Operation
        get() {
            return when (operation) {
                Operation.Plus -> Operation.Minus
                Operation.Minus -> Operation.Plus
                Operation.Division -> Operation.Multiply
                Operation.Multiply -> if (secondOperand != 0.0) Operation.Division else Operation.Plus
            }
        }

    fun getOperand() =
        if (operation == Operation.Multiply && secondOperand == 0.0) firstOperand
        else secondOperand
}