package com.hanaa.simplecalculator.data


sealed class Operation {
    abstract val sign: String

    object Plus : Operation() {
        override val sign = "+"

    }

    object Minus : Operation() {
        override val sign = "-"
    }

    object Multiply : Operation() {
        override val sign = "*"
    }

    object Division : Operation() {
        override val sign = "/"


    }
}
