package com.hanaa.simplecalculator.data

import androidx.lifecycle.MutableLiveData

class CalculatorHistory {
    val undoList by lazy { MutableLiveData<ArrayList<OperationItem>>(ArrayList()) }
    var redoLastOperation: OperationItem? = null
}