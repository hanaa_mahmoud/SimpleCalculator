package com.hanaa.simplecalculator.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hanaa.simplecalculator.data.CalculatorHistory
import com.hanaa.simplecalculator.data.Operation
import com.hanaa.simplecalculator.data.OperationItem
import com.hanaa.simplecalculator.domain.interactor.PerformOperationUseCase
import com.hanaa.simplecalculator.presentation.util.roundNumber
import java.lang.IllegalArgumentException

class CalculatorViewModel(
    private val performOperationUseCase: PerformOperationUseCase
) : ViewModel() {
    val history: CalculatorHistory = CalculatorHistory()
    private val resultObservable = MutableLiveData(0.0)
    val secondOperandObservable = MutableLiveData<String?>()
    fun getResultObservable() = resultObservable as LiveData<Double>
    private var operation = MutableLiveData<Operation>()
    fun getOperationObservable() = operation as LiveData<Operation>
    val errorObservable by lazy { MutableLiveData<Throwable?>() }
//    private val equalObservable: LiveData<Boolean>
//        get() {
//            return Transformations.map(operation) {operation->
//                it != null
//            }
//        }

    private val equalObservable = MediatorLiveData<Boolean>()
    fun getEqualStatusObservable() = equalObservable as
            LiveData<Boolean>

    init {
        equalObservable.addSource(operation) {
            equalObservable.value = operation.value != null
                    && !secondOperandObservable.value.isNullOrEmpty()
        }
        equalObservable.addSource(secondOperandObservable) {
            equalObservable.value = operation.value != null
                    && !secondOperandObservable.value.isNullOrEmpty()
        }
    }


    fun performOperation(num: Double, isUndo: Boolean = false) {
        if (operation.value != null) {
            val operationItem =
                OperationItem(
                    resultObservable.value!!,
                    num.roundNumber()?:0.0,
                    operation.value!!
                )
            try {
                if (errorObservable.value != null) {
                    errorObservable.value = null
                }
                resultObservable.value = performOperationUseCase.getResult(operationItem)
                if (!isUndo) {
                    val newList = history.undoList.value?.apply {
                        add(0, operationItem)
                    }
                    history.undoList.value = (newList)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                errorObservable.value = e
            }
        } else {
            errorObservable.value = IllegalArgumentException("Please enter the operation")
        }
    }

    fun storeOperation(operation: Operation?) {
        this.operation.value = operation
    }

    fun getHistoryObservable() = history.undoList as LiveData<ArrayList<OperationItem>>
    fun getRedoOperation() = history.redoLastOperation
    fun undoLastOperation(position: Int = 0) {
        if (getHistoryObservable().value!!.size > position) {
            val undoOperationItem = getHistoryObservable().value?.removeAt(position)
            history.redoLastOperation = undoOperationItem
            history.undoList.value = getHistoryObservable().value
            operation.value = undoOperationItem?.undoOperation

            performOperation(undoOperationItem!!.getOperand(), true)

        }else{
            throw IllegalAccessException()
        }
    }

    fun redoOperation() {
        val undoOperationItem = getRedoOperation()
        history.redoLastOperation = null
        operation.value = undoOperationItem?.operation
        if(undoOperationItem!=null)
        performOperation(undoOperationItem.secondOperand)
        else
            throw IllegalAccessException()
    }

}