package com.hanaa.simplecalculator.presentation.util

import java.lang.Exception
import java.math.RoundingMode
import java.text.DecimalFormat

fun Double.roundNumber(format: String = "#.##"): Double? {
    val formatter = DecimalFormat(format)
        //.apply { roundingMode = RoundingMode.FLOOR }
    return try {

        formatter.format(this).toDouble()
    } catch (e: Exception) {
        null
    }
}