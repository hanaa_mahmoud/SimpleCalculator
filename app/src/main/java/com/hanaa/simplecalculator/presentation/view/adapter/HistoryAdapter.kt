package com.hanaa.simplecalculator.presentation.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hanaa.simplecalculator.R
import com.hanaa.simplecalculator.data.OperationItem
import kotlinx.android.synthetic.main.item_history.view.*

class HistoryAdapter(val mData: ArrayList<OperationItem> = ArrayList()) :
    RecyclerView.Adapter<HistoryAdapter.ItemViewHolder>() {
    interface OnCalculatorItemClickedListener {
        fun onClick(position: Int)
    }

    lateinit var onCalculatorItemClickedListener: OnCalculatorItemClickedListener
//        set(value) {
//            field = value
//            notifyDataSetChanged()
//        }

    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.setOnClickListener {
                if (::onCalculatorItemClickedListener.isInitialized) {
                    onCalculatorItemClickedListener.onClick(adapterPosition)
                }
            }
        }

        fun onBind(item: OperationItem) {
            itemView.itemButton.text = "${item.operation.sign}  ${item.secondOperand}"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_history, parent, false)
        )
    }

    override fun getItemCount() = mData.size
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.onBind(mData[position])
    }
}