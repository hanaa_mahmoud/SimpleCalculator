package com.hanaa.simplecalculator.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.hanaa.simplecalculator.domain.interactor.PerformOperationUseCase

class CalculatorViewModelFactory(private val performOperationUseCase: PerformOperationUseCase) :
    ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CalculatorViewModel(performOperationUseCase = performOperationUseCase) as T
    }
}