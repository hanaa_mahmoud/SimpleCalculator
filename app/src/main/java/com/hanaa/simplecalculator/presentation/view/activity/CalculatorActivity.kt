package com.hanaa.simplecalculator.presentation.view.activity

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.hanaa.simplecalculator.R
import com.hanaa.simplecalculator.data.Operation
import com.hanaa.simplecalculator.domain.exception.DivideByZeroException
import com.hanaa.simplecalculator.domain.exception.InfiniteNumberException
import com.hanaa.simplecalculator.domain.exception.NotNumberException
import com.hanaa.simplecalculator.domain.interactor.PerformOperationUseCase
import com.hanaa.simplecalculator.presentation.view.adapter.HistoryAdapter
import com.hanaa.simplecalculator.presentation.viewmodel.CalculatorViewModel
import com.hanaa.simplecalculator.presentation.viewmodel.CalculatorViewModelFactory
import kotlinx.android.synthetic.main.activity_calculator.*

class CalculatorActivity : AppCompatActivity(), HistoryAdapter.OnCalculatorItemClickedListener {
    lateinit var viewModel: CalculatorViewModel
    lateinit var adapter: HistoryAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculator)
        viewModel =
            ViewModelProvider(this, CalculatorViewModelFactory(PerformOperationUseCase())).get(
                CalculatorViewModel::class.java
            )
        setupRecyclerView()
        addButtonsActionListeners()
        bindResultObserver()
        bindOperationButtons()
        bindSecondOperandChange()
        bindEqualButton()
        bindError()
    }

    private fun bindError() {
        viewModel.errorObservable.observe(this) {
            it?.run {
                Toast.makeText(
                    this@CalculatorActivity, getErrorMessage(it), Toast.LENGTH_LONG
                ).show()
                viewModel.errorObservable.value = null
            }
        }
    }

    private fun getErrorMessage(throwable: Throwable): String {
        return when (throwable) {
            is DivideByZeroException -> getString(R.string.cant_divide_by_zero)
            is InfiniteNumberException -> {
                if (throwable.isResult)
                    getString(R.string.result_size_exceeded_the_allowed_size_limit)
                else getString(R.string.number_size_exceeded_the_allowed_size_limit)
            }
            is NotNumberException -> getString(R.string.second_operand_not_a_number)
            else -> throwable.localizedMessage
        }.toString()
    }

    private fun bindSecondOperandChange() {
        secondOperandEditText.doOnTextChanged { text,
                                                _,
                                                _,
                                                _ ->
            viewModel.secondOperandObservable.value = text.toString()

        }
    }

    private fun bindEqualButton() {
        viewModel.getEqualStatusObservable().observe(this) {
            equalButton.isEnabled = it
        }
    }

    private fun bindResultObserver() {
        viewModel.getResultObservable().observe(this) {
            resultTextView.text = it.toString()//getString(R.string.result, it)
            secondOperandEditText.text?.clear()
            viewModel.storeOperation(null)
        }
    }

    private fun addButtonsActionListeners() {
        plusButton.setOnClickListener {
            storeOperation(Operation.Plus)

        }
        minusButton.setOnClickListener {
            storeOperation(Operation.Minus)
        }
        multiplyButton.setOnClickListener { storeOperation(Operation.Multiply) }
        divisionButton.setOnClickListener { storeOperation(Operation.Division) }
        equalButton.setOnClickListener { performCalculation() }
        undoButton.setOnClickListener {
            try {

                viewModel.undoLastOperation()
            } catch (e: IllegalAccessException) {
                getString(R.string.illegal_operation)
            }
        }
        redoButton.setOnClickListener {
            try {

                viewModel.redoOperation()
            } catch (e: IllegalAccessException) {
                getString(R.string.illegal_operation)
            }
        }
    }

    private fun bindOperationButtons() {
        viewModel.getOperationObservable().observe(this) { operation ->
            plusButton.isEnabled = operation != Operation.Plus
            minusButton.isEnabled = operation != Operation.Minus
            multiplyButton.isEnabled = operation != Operation.Multiply
            divisionButton.isEnabled = operation != Operation.Division
        }
    }

    private fun storeOperation(operation: Operation) {
        viewModel.storeOperation(operation)
    }

    private fun performCalculation() {
        viewModel.performOperation(
            secondOperandEditText.text.toString().toDouble()
        )
    }

    private fun setupRecyclerView() {
        historyRecyclerView.layoutManager = GridLayoutManager(this, 4)
        adapter = HistoryAdapter(viewModel.getHistoryObservable().value!!)
        historyRecyclerView.adapter = adapter
        adapter.onCalculatorItemClickedListener = this
        viewModel.getHistoryObservable().observe(this) {
            adapter.notifyDataSetChanged()
            undoButton.isEnabled = it.size > 0
            redoButton.isEnabled = viewModel.getRedoOperation() != null
        }


    }

    override fun onClick(position: Int) {
        try {
            viewModel.undoLastOperation(position)
        } catch (e: IllegalAccessException) {
            getString(R.string.illegal_operation)
        }
    }
}