package com.hanaa.simplecalculator.domain.interactor

import com.hanaa.simplecalculator.data.Operation
import com.hanaa.simplecalculator.data.OperationItem
import com.hanaa.simplecalculator.domain.exception.DivideByZeroException
import com.hanaa.simplecalculator.domain.exception.InfiniteNumberException
import com.hanaa.simplecalculator.domain.exception.NotNumberException
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class PerformOperationUseCaseTest {
    lateinit var performOperationUseCase: PerformOperationUseCase

    @Before
    fun setUp() {
        if (!::performOperationUseCase.isInitialized)
            performOperationUseCase = PerformOperationUseCase()
    }

    @Test(expected = NotNumberException::class)
    fun getResultWithNANFirstOperandThenThrowNotNumberException() {
        val operationItem = OperationItem(Double.NaN, 1.1, Operation.Plus)
        performOperationUseCase.getResult(operationItem)
    }

    @Test(expected = NotNumberException::class)
    fun getResultWithNANSecondOperandThenThrowNotNumberException() {
        val operationItem = OperationItem(1.1, Double.NaN, Operation.Plus)
        performOperationUseCase.getResult(operationItem)
    }

    @Test(expected = InfiniteNumberException::class)
    fun getResultWithPositiveInfiniteFirstOperandThenThrowNotNumberException() {
        val operationItem = OperationItem(Double.POSITIVE_INFINITY, 1.1, Operation.Plus)
        performOperationUseCase.getResult(operationItem)
    }

    @Test(expected = InfiniteNumberException::class)
    fun getResultWithPositiveNegativeFirstOperandThenThrowNotNumberException() {
        val operationItem = OperationItem(Double.POSITIVE_INFINITY, 1.1, Operation.Plus)
        performOperationUseCase.getResult(operationItem)
    }

    @Test(expected = InfiniteNumberException::class)
    fun getResultWithPositiveInfiniteSecondOperandThenThrowNotNumberException() {
        val operationItem = OperationItem(1.1, Double.POSITIVE_INFINITY, Operation.Plus)
        performOperationUseCase.getResult(operationItem)
    }

    @Test(expected = InfiniteNumberException::class)
    fun getResultWithNegativeInfiniteSecondOperandThenThrowNotNumberException() {
        val operationItem = OperationItem(1.1, Double.NEGATIVE_INFINITY, Operation.Plus)
        performOperationUseCase.getResult(operationItem)
    }

    @Test(expected = InfiniteNumberException::class)
    fun getResultWithPositiveInfiniteResultThenThrowNotNumberException() {
        val operationItem = OperationItem(Double.MAX_VALUE, Double.MAX_VALUE, Operation.Plus)
        performOperationUseCase.getResult(operationItem)
    }

    @Test(expected = InfiniteNumberException::class)
    fun getResultWithNegativeInfiniteResultThenThrowNotNumberException() {
        val operationItem = OperationItem(-Double.MAX_VALUE, Double.MAX_VALUE, Operation.Minus)
        performOperationUseCase.getResult(operationItem)

    }

    @Test
    fun getResultWithPlusOperationThenGetTheSum() {
        val operationItem = OperationItem(10.0, 20.0, Operation.Plus)
        val result = performOperationUseCase.getResult(operationItem)
        assertEquals("", result!!, 30.0, 0.0)
    }

    @Test
    fun getResultWithMinusOperationThenGetTheDifference() {
        val operationItem = OperationItem(10.0, 20.0, Operation.Minus)
        val result = performOperationUseCase.getResult(operationItem)
        assertEquals("", result!!, -10.0, 0.0)
    }

    @Test
    fun getResultWithMultiplyOperationThenGetMultiplication() {
        val operationItem = OperationItem(10.0, 20.0, Operation.Multiply)
        val result = performOperationUseCase.getResult(operationItem)
        assertEquals("", result!!, 200.0, 0.0)
    }

    @Test
    fun getResultWithDivisionsOperationThenGetDivision() {
        val operationItem = OperationItem(10.0, 20.0, Operation.Division)
        val result = performOperationUseCase.getResult(operationItem)
        assertEquals("", result!!, 0.5, 0.0)
    }

    @Test(expected = DivideByZeroException::class)
    fun getResultWithDivisionsOperationAndZeroSecondOperandThenGetDivision() {
        val operationItem = OperationItem(10.0, 0.0, Operation.Division)
        performOperationUseCase.getResult(operationItem)
    }
}