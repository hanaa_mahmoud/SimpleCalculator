package com.hanaa.simplecalculator.presentation.util

import org.junit.Assert.assertTrue
import org.junit.Test

class NumberUtilKtTest {
    val number: Double? = 5.8896
    val roundedNumber = 5.89

    @Test
    fun roundNumberWithDoubleNumberThenReturnNumberWithTwoDecimalPoint() {
        assertTrue(number?.roundNumber() == roundedNumber)
    }

    @Test
    fun roundNumberWithMinDoubleNumberThenReturnNumber0() {
        assertTrue(Double.MIN_VALUE.roundNumber() == 0.0)
    }

    @Test
    fun roundNumberWithMaxDoubleNumberThenReturnNumberMax() {
        assertTrue(Double.MAX_VALUE.roundNumber() == Double.MAX_VALUE)
    }

    @Test
    fun roundNumberWithPositiveDoubleNumberThenReturnNumberNull() {
        assertTrue(Double.POSITIVE_INFINITY.roundNumber() == null)
    }

    @Test
    fun roundNumberWithNegativeDoubleNumberThenReturnNull() {
        assertTrue(Double.NEGATIVE_INFINITY.roundNumber() == null)
    }

    @Test
    fun roundNumberWithDoubleNumberAndCustomFormatThenReturnNumberWithinTheFormat() {
        assertTrue(1.25368.roundNumber("#.###") == 1.254)
    }
}