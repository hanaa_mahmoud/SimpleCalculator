package com.hanaa.simplecalculator.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.hanaa.simplecalculator.data.Operation
import com.hanaa.simplecalculator.data.OperationItem
import com.hanaa.simplecalculator.domain.exception.NotNumberException
import com.hanaa.simplecalculator.domain.interactor.PerformOperationUseCase
import com.nhaarman.mockitokotlin2.*
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test

class CalculatorViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()
    val resultObserver = mock<Observer<Double>>()
    val errorObserver = mock<Observer<Throwable?>>()
    val operationObserver = mock<Observer<Operation>>()
    val secondOperandObserver = mock<Observer<String?>>()
    val equalStatusObserver = mock<Observer<Boolean>>()
    val performOperationUseCase = mock<PerformOperationUseCase> {
        on { getResult(any()) } doReturn (1.1)
    }
    val performOperationWithErrorUseCase = mock<PerformOperationUseCase> {
        on { getResult(any()) } doAnswer { throw NotNumberException() }
    }

    /**
     * Test equal observable value
     * **/
    @Test
    fun equalWithEmptySecondOperandAndEmptyOperationThenFalse() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        // viewModel.secondOperandObservable.observeForever(secondOperandObserver)
        viewModel.getEqualStatusObservable().observeForever(equalStatusObserver)
        // viewModel.getOperationObservable().observeForever(operationObserver)
        viewModel.secondOperandObservable.value = ""
        viewModel.storeOperation(null)
        assertTrue(viewModel.getEqualStatusObservable().value == false)
    }

    @Test
    fun equalWithNullSecondOperandAndEmptyOperationThenFalse() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.getEqualStatusObservable().observeForever(equalStatusObserver)
        viewModel.secondOperandObservable.value = null
        viewModel.storeOperation(null)
        assertTrue(viewModel.getEqualStatusObservable().value == false)
    }

    @Test
    fun equalWithSecondOperandAndEmptyOperationThenFalse() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.getEqualStatusObservable().observeForever(equalStatusObserver)
        viewModel.secondOperandObservable.value = "5"
        viewModel.storeOperation(null)
        assertTrue(viewModel.getEqualStatusObservable().value == false)
    }

    @Test
    fun equalWithEmptySecondOperandAndOperationThenFalse() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.getEqualStatusObservable().observeForever(equalStatusObserver)
        viewModel.secondOperandObservable.value = ""
        viewModel.storeOperation(Operation.Plus)
        assertTrue(viewModel.getEqualStatusObservable().value == false)
    }

    @Test
    fun equalWithNullSecondOperandAndOperationThenFalse() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.getEqualStatusObservable().observeForever(equalStatusObserver)
        viewModel.secondOperandObservable.value = null
        viewModel.storeOperation(Operation.Plus)
        assertTrue(viewModel.getEqualStatusObservable().value == false)
    }

    @Test
    fun equalWithSecondOperandAndOperationThenFalse() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.getEqualStatusObservable().observeForever(equalStatusObserver)
        viewModel.secondOperandObservable.value = "5"
        viewModel.storeOperation(Operation.Plus)
        assertTrue(viewModel.getEqualStatusObservable().value == true)
    }

    /**
     * Test perform operation
     **/
    @Test
    fun performOperationWithOperationThenResultObservableInvoked() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.storeOperation(Operation.Plus)
        viewModel.getResultObservable().observeForever(resultObserver)
        viewModel.errorObservable.observeForever(errorObserver)
        viewModel.getOperationObservable().observeForever(operationObserver)
        viewModel.performOperation(1.1)
        verify(resultObserver, times(2)).onChanged(any())
        verify(errorObserver, times(0)).onChanged((any()))
        verify(operationObserver, times(1)).onChanged(any())

    }

    @Test
    fun performOperationWithNoOperationThenErrorObservableInvoked() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.getResultObservable().observeForever(resultObserver)
        viewModel.getOperationObservable().observeForever(operationObserver)
        viewModel.errorObservable.observeForever(errorObserver)
        viewModel.performOperation(1.1)
        verify(resultObserver, times(1)).onChanged(any())
        verify(errorObserver, times(1)).onChanged((any()))
        verify(operationObserver, times(0)).onChanged(any())

    }

    @Test
    fun performOperationWithNANErrorThenResultObservableInvoked() {
        val viewModel = CalculatorViewModel(performOperationWithErrorUseCase)
        viewModel.getResultObservable().observeForever(resultObserver)
        viewModel.errorObservable.observeForever(errorObserver)
        viewModel.getOperationObservable().observeForever(operationObserver)
        viewModel.storeOperation(Operation.Plus)
        viewModel.performOperation(1.1)
        verify(resultObserver, times(1)).onChanged(any())
        verify(errorObserver, times(1)).onChanged((any()))
        verify(operationObserver, times(1)).onChanged(any())

    }

    /**
     * Test Adding result @ TODOList after perform any operation
     * **/
    @Test
    fun performOperationWithUndoISFalseThenValueAddedToUnDoList() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.storeOperation(Operation.Plus)
        viewModel.performOperation(1.1)
        assertEquals(viewModel.history.undoList.value?.size, 1)

    }

    @Test
    fun performOperationWithUndoISTrueThenValueNotAddedToUnDoList() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.storeOperation(Operation.Plus)
        viewModel.performOperation(1.1, true)
        assertEquals(viewModel.history.undoList.value?.size, 0)

    }

    /**
    Test Store Oprtation
     **/
    @Test
    fun storeOperationWithPlusOperationThenOperationObservableHavePlusValue() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.getOperationObservable().observeForever(operationObserver)
        viewModel.storeOperation(Operation.Plus)
        verify(operationObserver, times(1)).onChanged(Operation.Plus)
        //   assertEquals(viewModel.getOperationObservable().value, Operation.Plus)

    }

    @Test
    fun storeOperationWithEmptyOperationThenClearOperationObservable() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.getOperationObservable().observeForever(operationObserver)
        viewModel.storeOperation(null)
        verify(operationObserver, times(1)).onChanged(null)

    }

    /**
     * Test redo operation
     **/
    @Test(expected = IllegalAccessException::class)
    fun redoOperationWithEmptyRedoListThenThrowException() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.redoOperation()
    }

    @Test
    fun redoOperationThenRemoveOperationFromRedo() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.history.redoLastOperation = OperationItem(1.1, 1.1, Operation.Plus)
        viewModel.redoOperation()
        assertTrue(viewModel.history.redoLastOperation == null)
    }

    @Test
    fun redoOperationThenAddOperationToUndo() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.history.redoLastOperation = OperationItem(1.1, 1.1, Operation.Plus)
        viewModel.redoOperation()
        assertTrue(viewModel.history.undoList.value?.size == 1)
    }

    @Test
    fun redoOperationThenOperationBeOperationItemOperation() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.getOperationObservable().observeForever(operationObserver)
        val operationItem = OperationItem(1.1, 1.1, Operation.Plus)
        viewModel.history.redoLastOperation = operationItem
        viewModel.redoOperation()
        assertTrue(viewModel.getOperationObservable().value == operationItem.operation)
    }

    /**
     * Test undo operation
     **/

    @Test(expected = IllegalAccessException::class)
    fun undoOperationWithEmptyUndoListThenThrowException() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.undoLastOperation()
    }

    @Test(expected = IllegalAccessException::class)
    fun undoOperationWithPositionGreaterThanSizeThenThrowException() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.history.undoList.value = getUndoOperations()
        viewModel.undoLastOperation(5)
    }

    private fun getUndoOperations(): ArrayList<OperationItem> {

        val list = ArrayList<OperationItem>()
        list.add(OperationItem(1.1, 2.3, Operation.Plus))
        list.add(OperationItem(1.1, 2.3, Operation.Division))
        list.add(OperationItem(1.1, 2.3, Operation.Multiply))

        return list
    }

    @Test
    fun undoOperationThenRemoveOperationFromUndo() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.history.undoList.value = getUndoOperations()
        val size = viewModel.history.undoList.value?.size ?: 0
        viewModel.undoLastOperation()
        assertTrue(viewModel.history.undoList.value?.size == size - 1)
    }

    @Test
    fun undoOperationThenAddOperationToRedo() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        viewModel.history.undoList.value = getUndoOperations()
        val operationItem = viewModel.history.undoList.value?.get(0)
        viewModel.undoLastOperation()
        assertTrue(viewModel.history.redoLastOperation == operationItem)
    }

    @Test
    fun undoOperationWithSpecificPositionThenAddOperationToRedo() {
        val viewModel = CalculatorViewModel(performOperationUseCase)
        val pos = 2
        viewModel.history.undoList.value = getUndoOperations()
        val operationItem = viewModel.history.undoList.value?.get(pos)
        viewModel.undoLastOperation(pos)
        assertTrue(viewModel.history.redoLastOperation == operationItem)
    }

}