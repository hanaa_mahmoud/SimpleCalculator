package com.hanaa.simplecalculator.data

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class OperationItemTest {
    /***
    Test UndoOperation property value
     ***/
    @Test
    fun getUndoOperationWithPlusOperationAndNonZeroSecondOperandThenMinusOperation() {
        //Arrange
        val operationItem = OperationItem(1.0, 2.0, Operation.Plus)
        //act
        val operation = operationItem.undoOperation
        //Assert
        assertEquals(operation, Operation.Minus)

    }

    @Test
    fun getUndoOperationWithPlusOperationAndZeroSecondOperandThenMinusOperation() {
        //Arrange
        val operationItem = OperationItem(1.0, 0.0, Operation.Plus)
        //act
        val operation = operationItem.undoOperation
        //Assert
        assertEquals(operation, Operation.Minus)

    }

    @Test
    fun getUndoOperationWithMinusOperationAndNonZeroSecondOperandThenPlusOperation() {
        //Arrange
        val operationItem = OperationItem(1.0, 2.0, Operation.Minus)
        //act
        val operation = operationItem.undoOperation
        //Assert
        assertEquals(operation, Operation.Plus)

    }

    @Test
    fun getUndoOperationWithMinusOperationAndZeroSecondOperandThenPlusOperation() {
        //Arrange
        val operationItem = OperationItem(1.0, 0.0, Operation.Minus)
        //act
        val operation = operationItem.undoOperation
        //Assert
        assertEquals(operation, Operation.Plus)

    }

    @Test
    fun getUndoOperationWithDivisionOperationAndNonZeroSecondOperandThenMultiplyOperation() {
        //Arrange
        val operationItem = OperationItem(1.0, 2.0, Operation.Division)
        //act
        val operation = operationItem.undoOperation
        //Assert
        assertEquals(operation, Operation.Multiply)

    }

    @Test
    fun getUndoOperationWithDivisionOperationAndZeroSecondOperandThenMultiplyOperation() {
        //Arrange
        val operationItem = OperationItem(1.0, 0.0, Operation.Division)
        //act
        val operation = operationItem.undoOperation
        //Assert
        assertEquals(operation, Operation.Multiply)

    }


    @Test
    fun getUndoOperationWithMultiplyAndNonZeroSecondOperandOperationThenDivisionOperation() {
        //Arrange
        val operationItem = OperationItem(1.0, 2.0, Operation.Multiply)
        //act
        val operation = operationItem.undoOperation
        //Assert
        assertEquals(operation, Operation.Division)

    }

    @Test
    fun getUndoOperationWithMultiplyAndZeroSecondOperandOperationThenDivisionOperation() {
        //Arrange
        val operationItem = OperationItem(1.0, 0.0, Operation.Multiply)
        //act
        val operation = operationItem.undoOperation
        //Assert
        assertEquals(operation, Operation.Plus)

    }

    /***
    Test getOperand function return
     ***/

    @Test
    fun getOperandWithPlusOperationAndNonZeroSecondOperandThenSecondOperand() {
        val firstOperand = 1.0
        val secondOperand = 2.0
        //Arrange
        val operationItem = OperationItem(firstOperand, secondOperand, Operation.Plus)
        //act
        val operand = operationItem.getOperand()
        //Assert
        assertTrue(operand == secondOperand)

    }

    @Test
    fun getOperandWithPlusOperationAndZeroSecondOperandThenSecondOperand() {
        val firstOperand = 1.0
        val secondOperand = 0.0
        //Arrange
        val operationItem = OperationItem(firstOperand, secondOperand, Operation.Plus)
        //act
        val operand = operationItem.getOperand()
        //Assert
        assertTrue(operand == secondOperand)

    }

    @Test
    fun getOperandWithMinusOperationAndNonZeroSecondOperandThenSecondOperand() {
        val firstOperand = 1.0
        val secondOperand = 2.0
        //Arrange
        val operationItem = OperationItem(firstOperand, secondOperand, Operation.Minus)
        //act
        val operand = operationItem.getOperand()
        //Assert
        assertTrue(operand == secondOperand)

    }

    @Test
    fun getOperandWithMinusOperationAndZeroSecondOperandThenSecondOperand() {
        val firstOperand = 1.0
        val secondOperand = 0.0
        //Arrange
        val operationItem = OperationItem(firstOperand, secondOperand, Operation.Minus)
        //act
        val operand = operationItem.getOperand()
        //Assert
        assertTrue(operand == secondOperand)

    }

    @Test
    fun getOperandWithDivisionOperationAndNonZeroSecondOperandThenSecondOperand() {
        val firstOperand = 1.0
        val secondOperand = 2.0
        //Arrange
        val operationItem = OperationItem(firstOperand, secondOperand, Operation.Division)
        //act
        val operand = operationItem.getOperand()
        //Assert
        assertTrue(operand == secondOperand)

    }

    @Test
    fun getOperandWithDivisionOperationAndZeroSecondOperandThenSecondOperand() {
        val firstOperand = 1.0
        val secondOperand = 0.0
        //Arrange
        val operationItem = OperationItem(firstOperand, secondOperand, Operation.Division)
        //act
        val operand = operationItem.getOperand()
        //Assert
        assertTrue(operand == secondOperand)

    }

    @Test
    fun getOperandWithMultiplyAndNonZeroSecondOperandThenSecondOperand() {
        val firstOperand = 1.0
        val secondOperand = 2.0
        //Arrange
        val operationItem = OperationItem(firstOperand, secondOperand, Operation.Multiply)
        //act
        val operand = operationItem.getOperand()
        //Assert
        assertTrue(operand == secondOperand)

    }

    @Test
    fun getOperandWithMultiplyAndZeroSecondOperandThenSecondOperand() {
        val firstOperand = 1.0
        val secondOperand = 0.0
        //Arrange
        val operationItem = OperationItem(firstOperand, secondOperand, Operation.Multiply)
        //act
        val operand = operationItem.getOperand()
        //Assert
        assertTrue(operand == firstOperand)

    }
}